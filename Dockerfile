
# Build Stage - best practice for separating build and run time stage
# so the docker will create multiple temporary images during the build process, but keep only
# the latest image as the final artifact (to reduce the image size)
# 'cause there plenty dependencies useless in the run time so why keep them in the final version of the image
FROM node:20-alpine3.17 AS Build 

RUN mkdir -p /home/app

WORKDIR /home/app

COPY  package*.json .

# By using npm ci command we can install the same versions that we have in our local 
# machine in the node container.
RUN npm ci  

COPY ./src .
# RUN npm install 


# I have added this line just to test out building, and pushing a new version of docker image to dockerhub - the new version app:1.4 
# I updated the CMD instruction to see if there will be a changment
CMD ["node", "src/index.js"]


